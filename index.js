// hooks

const Timer = props => {
    const elapsed = useTimer();

    return <p>I have been running for {elapsed} seconds.</p>;
}

const useTimer = () => {
    const [elapsed, setElapsed] = React.useState(0);

    React.useEffect(() => {
        const handle = window.setInterval(() => setElapsed(elapsed + 1), 1000);
        return () => window.clearInterval(handle);
    });

    return elapsed;
}

ReactDOM.render(<Timer />, document.getElementById("root"));